# braintree_run
## Overview
This is a minimal implementation of the Braintree v.zero DropIn client for test and illustration purposes
[https://developers.braintreepayments.com/javascript+node/start/overview](Get Started with Braintree).  The demo is implemented two ways for illustrative purposes.  The first way uses explicit APIs `payments/client_token.js` and `payments/payments_methods.js` on the AWS server that communicate with the Braintree gateway.  The second way uses an API `payments/requests.js` that essentially makes all requests supported by the Braintree gateway accessible to the client.

## Use with AWS
Some details:

1. This repo is the top level integration module that includes an Express router `api.js` that link the request handlers in `braintree_app` with the Express server in `braintree_server`.
2.

## Braintree Configuration
The handlers from `braintree_app` that access the Braintree gateway must be configured with a Braintree configuration object of the form 
```
configBt = {
	merchantId: *Merchant ID*,
	publicKey: *Public Key*,
	privateKey: *Private Key*
}
```
The configuration object is initialized from environment variables which typically are set by a shell script `braintree.sh`
```
export BRAINTREE_MERCHANT_ID=*Merchant ID*
export BRAINTREE_PUBLIC_KEY=*Public Key*
export BRAINTREE_PRIVATE_KEY=*Private Key*
```
The Braintree credentials can be found in the [https://sandbox.braintreegateway.com/login](Braintree Dashboard) under "Authorization" item of the **Account** -> **My user** selection.

The script that sets the environment variables must be run before the app is started, e.g.
```
$ source braintree.shh
```


## Backendless Configurationn
The handlers from `braintree_app` verify the current logged in user against an potentially independent Backendless instance that handles users.  These handlers must be configured with a Backendless configuration object of the form
```
configBkUser = {	
    appId: *Application ID*,
    javascriptSecretKey: *Javascript Secret Key*,
    restSecretKey: *REST Secret Key*,
    appVersion: "v1"
```
The configuration object is initialized from environment variables which typically are set by a shell script `braintree.sh`:
```
export BACKENDLESS_APP_ID=*Application ID*
export BACKENDLESS_JAVASCRIPT_SECRET_KEY=*Javascript Secret Key*
export BACKENDLESS_REST_SECRET_KEY=*REST Secret Key*
export BACKENDLESS_APP_VERSION=v1
```
The Backendless credentials can be found in Backendless Developers console under **Manage** -> **App Settings** selection for the app..
The script that sets the environment variables must be run before the app is started, e.g
```
$ source braintree.sh
```

Following Node.js and npm conventions for now, the `config` folder and shell files are not included in the repo.

## Building a local version of the app for testing and development.

**1. Clone all three repos:**
```
$ cd /path/to/folder/for/repos
$ git clone https://bitbucket.org/backfliptech/braintree_server.git
$ git clone https://bitbucket.org/backfliptech/braintree_test.git
$ git clone https://bitbucket.org/backfliptech/braintree_app.git
```

**2) Install the dependencies for each node.js projects:**
```
$ cd ../braintree_server
$ npm install

$ cd ../braintree_app
$ (sudo) npm link

$ cd ../braintree_test
$ npm install
$ npm install https://bitbucket.org/backfliptech/braintree_server.git
$ npm link braintree_app
```

**3) Create the Backendless and Braintree configuration info**
Create a `config` folder and the `braintree.ah` and `backendless_user.sh` configuration files in that folder.  These files must be source'd to make the environment variables available
```
$ source backendless_user.sh
$ source brainree.sh
```

**4) Create a local TLS/SSL certificate**
This is for use with https: in the `braintree_test` project: (see the `README.md` in the `braintree_server` app).**
```
$ cd ../braintree_test
$ mkdir cert
$ cd cert
$ openssl genrsa -out key.pem 2048
$ openssl req -new -sha256 -key key.pem -out csr.pem
$ openssl x509 -req -in csr.pem -signkey key.pem -out certificate.pem
```
Note 1: In the step creating the `csr.pem` answer the question:
```
Common Name (e.g. server FQDN or YOUR name) []:localhost
```
Note 2: It may be necessary to regenerate these files after Dec. 31 of each year for the next year.

**5) Create an environment setup file**
The local version of the project runs as http: application on port 3000. If desired, create a local shell script file ``default_env.sh` in the `braintree_test` to set environment variables:
```
#!/bin/sh
#
export PROTOCOL=https
export PORT=3000
```

and run it:
```
$ cd ../braintree_test
$ source default_env.sh
```

## Run the app as a local project
```
$ cd ../braintree_test
$ node index.js
```



