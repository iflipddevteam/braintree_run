/**
 * Router for this app
 */
;(function() {
	
//	var configBkUser = require('./config/backendless_user.js');
//	var configBt = require('./config/braintree.js');
	
	// Configuration is read from environment variables that must be set before invocation
	var configBkUser = {};
	if(process.env.BACKENDLESS_APP_ID)
		configBkUser.appId = process.env.BACKENDLESS_APP_ID;
	if(process.env.BACKENDLESS_JAVASCRIPT_SECRET_KEY)
		configBkUser.javascriptSecretKey = process.env.BACKENDLESS_JAVASCRIPT_SECRET_KEY;
	if(process.env.BACKENDLESS_REST_SECRET_KEY)
		configBkUser.restSecretKey = process.env.BACKENDLESS_REST_SECRET_KEY;
	if(process.env.BACKENDLESS_APP_VERSION)
		configBkUser.appVersion = process.env.BACKENDLESS_APP_VERSION;
	
	var configBt = {};
	if(process.env.BRAINTREE_MERCHANT_ID)
		configBt.merchantId = process.env.BRAINTREE_MERCHANT_ID;
	if(process.env.BRAINTREE_PUBLIC_KEY)
		configBt.publicKey = process.env.BRAINTREE_PUBLIC_KEY;
	if(process.env.BRAINTREE_PRIVATE_KEY)
		configBt.privateKey = process.env.BRAINTREE_PRIVATE_KEY;	
	
	var express = require('express')
	  , util = require('util')
	  , path = require('path')
	  , bodyParser = require('body-parser')
	  , debug = require('debug')('api')
	  
	  , btS = 'braintree_server'
	  , common = require(btS).lib.common
	  
	  , btA = 'braintree_app'
	  , braintree_app = require(btA)
	  , hello = require(btA + '/hello.js').run
	  , client_token = require(btA + '/payments/client_token.js').config(configBt).run
	  , payment_methods = require(btA + '/payments/payment_methods.js').config(configBt).run

	  , ping = require(btA + '/monitor/ping.js').run
	  , ping_auth = require(btA + '/monitor/ping_auth.js').config(configBkUser).run
	  
/*
 * uncomment to enable cron job
 */
	  , update_status_request = require(btA + '/cron/update_status.js').config(configBkUser, configBt).run
	  , update_status2_request = require(btA + '/cron/update_status2.js').config(configBkUser, configBt).run
	  , update_status_auth_request = require(btA + '/cron/update_status_auth.js').config(configBkUser, configBt).run

	  , gateway_request = require(btA + '/payments/gateway.js').config(configBt).run
	  , gateway2_request = require(btA + '/payments/gateway2.js').config(configBt).run
	  , gateway_auth_request = require(btA + '/payments/gateway_auth.js').config(configBkUser, configBt).run;	 


	var api = function(app, options) {
		
		var routes = express.Router();

		var urlencodedParser = bodyParser.urlencoded({extended:false});		
		routes.use(urlencodedParser);
		
		var textParser = bodyParser.text({type:"application/json"});
		routes.use(textParser);
		
		var defaultParser = bodyParser.text({type:"text/plain"});
		routes.use(defaultParser);
		
//		var jsonParser = bodyParser.json();
//		routes.use(jsonParser);
		
		// test script
		routes.use('/hello.js', hello);
		
		// pings
		routes.use('/monitor/ping_auth.js', ping_auth);
		routes.use('/monitor/ping.js', ping);

		// script requests
		routes.get('/payments/client_token.js', client_token);
		routes.post('/payments/payment_methods.js', urlencodedParser, payment_methods);
		
		routes.use('/payments/gateway.js', gateway_request);		
		routes.use('/payments/gateway2.js', gateway2_request);		
		routes.use('/payments/gateway_auth.js', gateway_auth_request);
/*
 * uncomment to enable cron job
 */ 
		routes.use('/cron/update_status.js', update_status_request);
		routes.use('/cron/update_status2.js', update_status2_request);
		routes.use('/cron/update_status_auth.js', update_status_auth_request);

//		routes.post('/payments/request/urlencoded/:request/:method', urlencodedParser, gateway_request.post);

		// static resource requests would get handled here
		routes.use('/public', express['static'](path.resolve(braintree_app.dirname(), './public'),
				{'redirect': true}));

		// hang the subrouter on the app.
		// It seems this could improve route matching performance for the different sub-apis
//		app.use('/braintree_app', wsAuth, routes);
		app.use('/', routes);
	};
	
	module.exports = api;

}).call(this);